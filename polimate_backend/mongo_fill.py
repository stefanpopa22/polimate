from mongo_querier import MongoQuerier

db = MongoQuerier()


def fill_users():
    user1 = {
        "name": "Bob Green",
        "email": "bob.green@gmail.com",
        "phone": "+40731654213",
        "photo": "resources/img/profile_picture1.jpeg"
    }
    user2 = {
        "name": "Kim Rushford",
        "email": "kimrush@gmail.com",
        "phone": "+40731322134",
        "photo": "resources/img/profile_picture3.jpeg"
    }

    db.post_user(user1)
    db.post_user(user2)


def fill_projects():
    project1 = {
        'title': 'Shopping Web Platform',
        'employer_email': 'bob.green@gmail.com',
        'details': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tempus augue ut purus facilisis, id accumsan lacus hendrerit. In semper pulvinar mollis. Nam mollis risus massa, et sagittis mi porttitor eu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras dolor justo, maximus vitae libero ut, elementum luctus felis. Pellentesque dui sem, sagittis ac tincidunt quis, convallis sit amet metus. Nulla varius, purus non molestie feugiat, turpis ligula suscipit lectus, non imperdiet justo nisl ac neque. Nullam massa nisl, egestas sed consequat ut, aliquet non neque. Phasellus dictum malesuada massa.',
        'skills': ['HTML', 'CSS', 'JavaScript'],
        'budget': 1000,
        'bids': [],
        'deadline': 'Mon, 25 Dec 2018'
    }
    project2 = {
        'title': 'Dummy project1',
        'employer_email': 'kimrush@gmail.com',
        'details': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tempus augue ut purus facilisis, id accumsan lacus hendrerit. In semper pulvinar mollis. Nam mollis risus massa, et sagittis mi porttitor eu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras dolor justo, maximus vitae libero ut, elementum luctus felis. Pellentesque dui sem, sagittis ac tincidunt quis, convallis sit amet metus. Nulla varius, purus non molestie feugiat, turpis ligula suscipit lectus, non imperdiet justo nisl ac neque. Nullam massa nisl, egestas sed consequat ut, aliquet non neque. Phasellus dictum malesuada massa.',
        'skills': ['C', 'C++'],
        'budget': 1500,
        'bids': [],
        'deadline': 'Mon, 25 Dec 2018'
    }

    for project in [project1, project2]:
        db.post_project(project)


def main():
    # fill_users()
    # fill_projects()
    pass


if __name__ == "__main__":
    main()