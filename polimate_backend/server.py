import random
from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin
from werkzeug.utils import secure_filename
import json
import os
from mongo_querier import MongoQuerier

app = Flask(__name__)
CORS(app, supports_credentials=True)
api = Api(app)

db_querier = MongoQuerier()

class Projects(Resource):
    def get(self):
        return db_querier.get_projects()

    def post(self):
        user = request.cookies.get('polimate_user')
        token = request.cookies.get('polimate_token')

        if not db_querier.check_credential_token(user, token) or \
            len(db_querier.get_user(user)) == 0:
            return {
                'status': 'error',
                'message': 'Invalid session'
            }

        title = request.form['title']
        details = request.form['details']
        deadline = request.form['deadline']
        skills = json.loads(request.form['skills'])
        budget = request.form['budget']
        user_entry = db_querier.get_user(user)[0]

        db_querier.post_project({
            'title': title,
            'employer_email': user_entry['email'],
            'details': details,
            'skills': skills,
            'budget': int(budget),
            'bids': [],
            'deadline': deadline
        });

        return {'status': 'success'}


class Bids(Resource):
    def get(self):
        return "Get bids"

    def post(self):
        project_id = request.form['project_id'];
        bid = json.loads(request.form['bid'])

        db_querier.post_bid(project_id, bid)
        return {'status': 'success'}


class Upload(Resource):
    def post(self):
        server_filename = request.form['server_filename']
        file = request.files['file']
        filename = secure_filename(file.filename)
        _, file_extension = os.path.splitext(filename)
        server_filename += file_extension
        file.save(os.path.join(os.getcwd() + "/../resources/img", server_filename))

        return {'status': 'success', 'server_filename': server_filename}


class Users(Resource):
    def get(self, user_email=""):
        if user_email:
            return db_querier.get_user(user_email)
        return db_querier.get_users()

    def post(self, user_email=""):
        if not user_email:
            # Registration
            name = request.form['name']
            password_hash = request.form['password_hash']
            email = request.form['email']
            last_active = request.form['last_active'];

            if len(self.get(email)) > 0:
                return {
                    'status': 'error',
                    'message': 'A user with this email address already exists.'
                }

            db_querier.post_user({
                'name': name,
                'email': email,
                'phone': '',
                'photo': 'resources/img/default_profile_picture.png',
                'about_me': '',
                'skills': [],
                'lives_in': '',
                'last_active': last_active,
                'added_projects': 0,
                'num_bids': 0
            })

            db_querier.post_credential({
                'user': email,
                'password_hash': password_hash
            })

            return {'status': 'success'}

        # Update
        _ = request.form['update']
        to_update = {}
        for field in ['name', 'phone', 'photo', 'about_me', 'skills', 'lives_in', 'last_active', 'added_projects', 'num_bids']:
            if field in request.form:
                to_update[field] = request.form[field]

        db_querier.update_user(user_email, to_update)

        return {'status': 'success'}


class Login(Resource):
    def post(self):
        email = request.form['email']
        password_hash = request.form['password_hash']

        credential = db_querier.get_credential(email)
        if len(credential) == 0:
            return {'status': 'error', 'message': 'No user is registered with this email.'}

        if credential[0]['password_hash'] != password_hash:
            return {'status': 'error', 'message': 'Incorrect password.'}

        token = hex(random.getrandbits(256)).rstrip("L").lstrip("0x");
        db_querier.update_credential_token(email, token)

        return {'status': 'success', 'token': token}


api.add_resource(Projects, '/projects')
api.add_resource(Bids, '/bids')
api.add_resource(Users, '/users', '/users/<string:user_email>')
api.add_resource(Upload, '/upload')
api.add_resource(Login, '/login')

if __name__ == '__main__':
     app.run(threaded=True, port=5002)