#!/bin/bash

mkdir venv &> /dev/null
virtualenv venv && source venv/bin/activate && pip install -r requirements.txt
