from pymongo import MongoClient
from bson import json_util
from bson.objectid import ObjectId
import json

def serialize(data):
    return json.loads(json_util.dumps(data))


class MongoQuerier:
    def __init__(self):
        self.mongo_client = MongoClient("mongodb+srv://stefanzzz:polimate_rulz@cluster0-um11e.mongodb.net/test")
        self.db = self.mongo_client.polimate

    def post_project(self, project):
        self.db.projects.insert_one(project)

    def get_projects(self):
        projects = []
        for project in self.db.projects.find():
            projects.append(project)
        return serialize(projects)

    def post_bid(self, project_id, bid):
        self.db.projects.update_one({"_id": ObjectId(project_id)}, {"$push": {"bids": bid}})

    def post_user(self, user):
        self.db.users.insert_one(user)

    def get_users(self):
        users = []
        for user in self.db.users.find():
            users.append(user)
        return serialize(users)

    def get_user(self, user_email):
        users = []
        for user in self.db.users.find({"email": user_email}):
            users.append(user)
        return serialize(users)

    def update_user(self, user_email, update_dict):
        self.db.users.update_one({'email': user_email}, {"$set": update_dict})

    def get_credential(self, user):
        credentials = []
        for credential in self.db.credentials.find({"user": user}):
            credentials.append(credential)
        return serialize(credentials)

    def check_credential_token(self, user, token):
        for credential in self.get_credential(user):
            if credential['token'] == token:
                return True
        return False

    def post_credential(self, credential):
        self.db.credentials.insert_one(credential)

    def update_credential_token(self, user, token):
        self.db.credentials.update_one({'user': user}, {"$set": {"token": token}})


if __name__ == "__main__":
    # This is only an example of usage. We shouldn't execute this script directly but rather
    # use a MongoQuerier object in server.py.
    project = {
        'title': 'Shopping Web Platform',
        'employer_email': 'bob.green@gmail.com',
        'details': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tempus augue ut purus facilisis, id accumsan lacus hendrerit. In semper pulvinar mollis. Nam mollis risus massa, et sagittis mi porttitor eu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras dolor justo, maximus vitae libero ut, elementum luctus felis. Pellentesque dui sem, sagittis ac tincidunt quis, convallis sit amet metus. Nulla varius, purus non molestie feugiat, turpis ligula suscipit lectus, non imperdiet justo nisl ac neque. Nullam massa nisl, egestas sed consequat ut, aliquet non neque. Phasellus dictum malesuada massa.',
        'skills': ['HTML', 'CSS', 'JavaScript'],
        'budget': 1000,
        'bids': [],
        'deadline': 'Mon, 25 Dec 1995 13:30:00'
    }
    querier = MongoQuerier()
    print(querier.get_projects())