'use strict';

var skills = ["4th Dimension/4D", "ABAP", "ABC", "ActionScript", "Ada", "Agilent VEE", "Algol", "Alice", "Apex",
              "APL", "AppleScript", "Arc", "Arduino", "ASP", "AspectJ", "Assembly", "ATLAS", "Augeas",
              "AutoHotkey", "AutoIt", "AutoLISP", "Automator", "Avenue", "Awk", "Bash", "(Visual) Basic",
              "bc", "BCPL", "BETA", "BlitzMax", "Boo", "Bourne Shell", "Bro", "C", "C Shell", "C#", "C++",
              "C++/CLI", "Caml", "Ceylon", "CFML", "cg", "Ch", "CHILL", "CIL", "CL (OS/400)", "Clarion",
              "Clean", "Clipper", "Clojure", "CLU", "COBOL", "Cobra", "CoffeeScript", "ColdFusion", "COMAL",
              "Common Lisp", "Coq", "CSS", "Curl", "D", "Dart", "DCL", "DCPU-16 ASM", "Delphi/Object Pascal",
              "DiBOL", "Dylan", "E", "Ecl", "ECMAScript", "EGL", "Eiffel", "Emacs Lisp", "Erlang", "Etoys",
              "Euphoria", "EXEC", "F#", "Factor", "Falcon", "Fancy", "Fantom", "Felix", "Forth", "Fortran",
              "Fortress", "(Visual) FoxPro", "Gambas", "GNU Octave", "Go", "Google AppsScript", "Gosu",
              "Groovy", "Haskell", "haXe", "HTML", "HyperTalk", "Icon", "IDL", "Inform", "Informix-4GL", "INTERCAL",
              "Io", "Ioke", "J", "J#", "JADE", "Java", "Java FX Script", "JavaScript", "JScript", "JScript.NET",
              "Korn Shell", "Kotlin", "LabVIEW", "Ladder Logic", "Lasso", "Limbo", "Lingo", "Lisp", "Logo",
              "Logtalk", "LotusScript", "LPC", "Lua", "Lustre", "M4", "MAD", "Magic", "Magik", "Malbolge",
              "MANTIS", "Maple", "Mathematica", "MATLAB", "Max/MSP", "MAXScript", "MEL", "Mercury", "Mirah",
              "Miva", "ML", "Monkey", "Modula-2", "Modula-3", "MOO", "MS-DOS Batch", "MUMPS", "NATURAL",
              "Nemerle", "Nimrod", "NQC", "NSIS", "Nu", "NXT-G", "Oberon", "Object Rexx", "Objective-C",
              "Objective-J", "OCaml", "Occam", "Opa", "OpenCL", "OpenEdge ABL", "OPL", "Oz", "Paradox", "Parrot",
              "Pascal", "Perl", "PHP", "Pike", "PILOT", "PL/I", "PL/SQL", "PostScript", "POV-Ray", "PowerBasic",
              "PowerScript", "PowerShell", "Processing", "Prolog", "Puppet", "Pure Data", "Python", "Q", "R",
              "Racket", "REALBasic", "REBOL", "Revolution", "REXX", "RPG (OS/400)", "Ruby", "Rust", "S", "S-PLUS",
              "SAS", "Sather", "Scala", "Scheme", "Scilab", "Scratch", "sed", "Seed7", "Self", "Shell", "SIGNAL",
              "Simula", "Simulink", "Smalltalk", "Smarty", "SPARK", "SPSS", "SQR", "Squeak", "Squirrel",
              "Standard ML", "Suneido", "SuperCollider", "TACL", "Tcl", "Tex", "thinBasic", "TOM", "Transact-SQL",
              "Turing", "TypeScript", "Vala", "VBScript", "Verilog", "VHDL", "VimL", "Visual Basic .NET", "WebDNA",
              "Whitespace", "X10", "xBase", "XBase++", "Xen", "XPL", "XSLT", "XQuery", "yacc", "Yorick", "Z shell"];

var autocomplete_data = {};

for (var i = 0; i < skills.length; i++) {
    autocomplete_data[skills[i]] = null;
}

var initSkillsFilter = function(skills_list) {
    $(".skills-input").autocomplete({
        data: autocomplete_data,
        limit: 10,
        onAutocomplete: function(skill) {
            skills_list.addSkill(skill);
            $(".skills-input").val("");
        }
    });

    $(".skills-input-chips").material_chip({
        autocompleteOptions: {
            data: autocomplete_data,
            limit: 10
        }
    });
}
