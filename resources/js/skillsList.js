'use strict'

function SkillsList(element, project_list) {
    this.element = element;
    this.project_list = project_list;

    this.skills = []
}

SkillsList.prototype.addSkill = function(skill) {
    if ($.inArray(skill, this.skills) > -1)
        return;

    this.skills.push(skill);
    $($.parseHTML(this.getSkillChipHTML(skill))).appendTo(this.element);
    this.project_list.refreshProjectList();
}

SkillsList.prototype.removeSkill = function(skill) {
    var idx = $.inArray(skill, this.skills)

    if (idx == -1)
        return;
    this.skills.splice(idx, 1);
    $(this.element).find(".skill-chip").filter(function() {
        return $(this).find(".chip > span").text() == skill;
    }).remove();
    this.project_list.refreshProjectList();
}

SkillsList.prototype.getSkillChipHTML = function(skill) {
    return `<div class='col s12 skill-chip'>
        <div class='chip full-width'>
            <span>` + skill + `</span>
            <div class='right'>
                <i class='fa fa-times clickable' onclick='app.project_list.skills_list.removeSkill("` + skill + `")'></i>
            </div>
        </div>
    </div>`;
}
