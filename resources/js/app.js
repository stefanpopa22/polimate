'use strict';

function App() {}

App.prototype.init = function() {
    var app = this;
    // Get the projects from the database
    $.ajax({
        type: "GET",
        url: backend_server + "/projects",
        dataType: "json"
    })
    .done(function(result) {
        projects = result;

        // Get the users from the database
        $.ajax({
            type: "GET",
            url: backend_server + "/users",
            dataType: "json"
        })
        .done(function(result) {
            users = {};
            for (var i = 0; i < result.length; ++i)
                users[result[i].email] = result[i];

            app.users_manager = new UsersManager();
            app.users_manager.tryLogin();
            app.setPage(defaultPage);
        });
    });
}

App.prototype.setPage = function(page) {
    var app = this;
    $("#page").load("resources/views/" + page + ".html", function() {
        app.pageLoadedCb(page);
    });
}

App.prototype.pageLoadedCb = function(page) {
    if (page == "projectsList") {
        this.project_list = new ProjectList($("#projects-list"), this);
        $(".collapsible").collapsible();
        initSkillsFilter(this.project_list.skills_list);
    }
    else if (page == "postProject") {
        this.post_project_form = new PostProjectForm($("#postProject"))
        initSkillsFilter();
    }
    else if (page == "login") {
        this.users_manager.setElement($("#login-form"));
    }
    else if (page == "project") {
        this.crt_project = new Project($("#project"), this);
    }
    else if (page == "profile") {
        this.profile = new Profile($("#profile-page"), this);
    }
}

App.prototype.openUserProfile = function(user_email) {
    displayed_user = user_email;
    this.setPage("profile");
}

var app = new App();

$(document).ready(function() {
    app.init();
});
