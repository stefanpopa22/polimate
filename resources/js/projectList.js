'use strict';

function ProjectList(element, app) {
    this.element = element;
    this.app = app;
    this.skills_list = new SkillsList(element.find(".skills-list")[0], this);
    this.init();
}

ProjectList.prototype.init = function() {
    this.refreshProjectList();
}

ProjectList.prototype.refreshProjectList = function() {
    var list_element = $(this.element).find('#project-list');

    list_element.find(".project-list-element").remove();

    var filtered_projects = this.getFilteredProjects();

    // Add the projects to the DOM
    for (var i = 0; i < filtered_projects.length; i++)
        list_element.append(this.getProjectHTML(filtered_projects[i]));
}

ProjectList.prototype.getFilteredProjects = function() {
    var skills = this.skills_list.skills;

    if (skills.length == 0)
        return projects;

    // Get filtered list of projects
    var filtered_projects = [];
    for (var i = 0; i < projects.length; i++) {
        var project = projects[i];
        project.numMatchedSkills = 0;

        for (var j = 0; j < skills.length; j++) {
            if (project.skills.indexOf(skills[j]) > -1)
                project.numMatchedSkills++;
        }

        if (project.numMatchedSkills > 0)
            filtered_projects.push(project);
    }

    // Sort the projecst decreasingly by number of matched skills
    filtered_projects.sort(function(p1, p2) {
        return p2.numMatchedSkills - p1.numMatchedSkills;
    });

    return filtered_projects;
}

ProjectList.prototype.openProject = function(project_oid) {
    for (var i = 0; i < projects.length; i++) {
        if (projects[i]._id.$oid == project_oid) {
            current_project = projects[i];
            current_project_employer = users[current_project.employer_email];
            break;
        }
    }
    this.app.setPage("project");
}

ProjectList.prototype.getProjectHTML = function(project) {
    var employer = users[project.employer_email]
    var average_bid = "No bids yet";

    if (project.bids.length > 0) {
        average_bid = 0;
        for (var i = 0; i < project.bids.length; ++i)
            average_bid += project.bids[i].value;
        average_bid /= project.bids.length;
        average_bid = average_bid.toFixed(2);
    }

    return `
    <li class="project-list-element">
        <div class="collapsible-header row">
          <div class="col s3">
            ` + project.title + `
          </div>
          <div class="col s3">
            ` + project.skills.join() + `
          </div>
          <div class="col s2">
            ` + project.budget + `
          </div>
          <div class="col s2">
            ` + average_bid + `
          </div>
          <div class="col s2">
            ` + project.deadline + `
          </div>
        </div>
        <div class="collapsible-body">
          <div class="row">
            <div class="col s7">
              <h6> <strong> Project Details </strong> </h6>
              <p> ` + project.details + `</p>
              <div class="right">
                <a class="waves-effect waves-light btn see-project-btn"
                  onclick="app.project_list.openProject('` + project._id.$oid + `')">
                  Go to project page <i class="fa fa-chevron-right"></i>
                </a>
              </div>
            </div>
            <div class="col s4 offset-s1 z-depth-1 grey lighten-5">
              <h6> <strong> Employer </strong> </h6>
              <br>
              <div class="row">
                <div class="col s3">
                  <img src="` + employer.photo + `" class="profile-picture circle">
                </div>
                <div class="col s8 offset-s1">
                  <h5 class="grey-text text-darken-3 margin-0">` + employer.name + `</h5>
                  <p class="grey-text margin-0 font-size-13">
                    <i class="fa fa-envelope"></i> &nbsp; ` + employer.email + `
                  </p>
                  <p class="grey-text margin-0">
                     <i class="fa fa-lg fa-mobile"></i> &nbsp; +40731654213
                  </p>
                    <a class="waves-effect waves-light btn right font-size-14" onclick="app.openUserProfile('` + employer.email + `')">
                      <i class="fa fa-search" style="font-size: 15px"></i>
                      See profile
                    </a>
                </div>
              </div>
            </div>
          </div>
        </div>
    </li>
      `;
}