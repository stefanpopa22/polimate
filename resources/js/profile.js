'use strict'

function Profile(element, app) {
    this.element = element;
    this.app = app;
    this.init();
}

Profile.prototype.init = function() {
    var user_data = users[displayed_user];

    if (user_data.phone == "")
        user_data.phone = "No phone added";
    if (user_data.about_me == "")
        user_data.about_me = "There's nothing else I have to say.";
    if (user_data.lives_in == "")
        user_data.lives_in = "No location added";

    this.element.find("input").hide();
    this.element.find("textarea").hide();
    this.element.find("#user-skills-input").show();
    this.element.find("#user-skills-input-div").hide();

    this.element.find("#user-photo").attr("src", user_data.photo);
    this.element.find("#user-name").text(user_data.name);
    this.element.find("#user-email").html(this.getUserEmailHtml(user_data.email));
    this.element.find("#user-phone").html(this.getUserPhoneHtml(user_data.phone));
    this.element.find("#user-added-projects").text(user_data.added_projects);
    this.element.find("#user-num-bids").text(user_data.num_bids);
    this.element.find("#user-about-me").text(user_data.about_me);
    var skills_element = this.element.find("#user-skills");
    if (user_data.skills.length == 0)
        skills_element.text("No skills added");
    else {
        for (var i = 0; i < user_data.skills.length; i++)
            skills_element.append(this.getSkillChipHtml(user_data.skills[i]));
    }
    this.element.find("#user-lives-in").text(user_data.lives_in);
    this.element.find("#user-last-active").html(this.getUserLastActiveHtml(user_data.last_active));

    if (displayed_user == g_logged_in_user)
        this.makeProfileEdtitable();
}

Profile.prototype.makeProfileEdtitable = function() {
    $('.editable').tooltip({delay: 50});
    $('.editable').addClass('clickable');

    var profile = this;

    // Make photo editable
    this.element.find("#user-photo-input").prop('disabled', false);    
    this.element.find("#user-photo-input").on('change', function() {
        var files = document.getElementById("user-photo-input").files;
        if (files.length == 0)
            return;
        var file = files[0];

        var formData = new FormData();
        formData.append("file", file, file.name);
        formData.append("upload_file", true);
        formData.append("server_filename", sha256(g_logged_in_user));

        $.ajax({
            type: "POST",
            url: backend_server + "/upload",
            xhr: function() {
                return $.ajaxSettings.xhr();
            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        }).done(function(result) {
            profile.updateUserInfo({"photo": "resources/img/" + result.server_filename})
        })
    });

    // Make name field editable
    this.element.find('#user-name').on('click', function() {
        $(this).hide();
        profile.element.find("#user-name-input").show();
    });
    this.element.find("#user-name-input").on("change", function() {
        var new_name = $(this).val();
        if (!new_name)
            return;
        $(this).val("");
        $(this).hide();
        profile.updateUserInfo({"name": new_name});
        profile.element.find("#user-name").show();
    });

    // Make phone field editable
    this.element.find('#user-phone').on('click', function() {
        $(this).hide();
        profile.element.find("#user-phone-input").show();
    });
    this.element.find("#user-phone-input").on("change", function() {
        var new_phone = $(this).val();
        if (!new_phone)
            return;
        $(this).val("");
        $(this).hide();
        profile.updateUserInfo({"phone": new_phone});
        profile.element.find("#user-phone").show();
    });

    // Make about-me field editable
    this.element.find('#user-about-me').on('click', function() {
        $(this).hide();
        profile.element.find("#user-about-me-input").show();
    });
    this.element.find("#user-about-me-input").on("change", function() {
        var new_about_me = $(this).val();
        if (!new_about_me)
            return;
        $(this).val("");
        $(this).hide();
        profile.updateUserInfo({"about_me": new_about_me});
        profile.element.find("#user-about-me").show();
    });

    // Make "lives in" field editable
    this.element.find('#user-lives-in').on('click', function() {
        $(this).hide();
        profile.element.find("#user-lives-in-input").show();
    });
    this.element.find("#user-lives-in-input").on("change", function() {
        var new_lives_in = $(this).val();
        if (!new_lives_in)
            return;
        $(this).val("");
        $(this).hide();
        profile.updateUserInfo({"lives_in": new_lives_in});
        profile.element.find("#user-lives-in").show();
    });
}

Profile.prototype.updateUserInfo = function(update_dict) {
    update_dict.update = "True";
    // Update in the database
    $.ajax({
        type: "POST",
        url: backend_server + "/users/" + g_logged_in_user,
        dataType: "json",
        data: update_dict
    });
    // Update locally
    for (var field in update_dict) {
        if (field != 'update')
            users[g_logged_in_user][field] = update_dict[field];
    }
    this.app.setPage('profile');
}

Profile.prototype.getUserEmailHtml = function(email) {
    return `<i class="fa fa-envelope"></i> &nbsp; ` + email;
}

Profile.prototype.getUserPhoneHtml = function(phone) {
    return `<i class="fa fa-lg fa-mobile"></i> &nbsp; &nbsp;` + phone;
}

Profile.prototype.getSkillChipHtml = function(skill) {
    return `<div class="chip"> ` + skill + ` </div>`;
}

Profile.prototype.getUserLastActiveHtml = function(last_active) {
    var d = new Date(Date.parse(last_active));
    var now = new Date();

    // If the user was active in the last 2 mins, consider it is active now
    var last_active_text;
    if (now - d < 5 * 60 * 1000) {
        last_active_text = "Active now";
        this.element.find("#user-last-active").removeClass("red-text");
        this.element.find("#user-last-active").addClass("green-text");
    }
    else
        last_active_text = d.toLocaleString();

    return last_active_text + `&nbsp; <i class="fa fa-circle font-size-11"> </i>`;
}
