'use strict';

function Project(element, app) {
    this.element = element;
    this.app = app;
    this.init();
}

Project.prototype.init = function() {
    this.element.find("#project-title").text(current_project.title);
    this.element.find("#project-details").text(current_project.details);
    var skills_element = this.element.find("#project-skills");
    for (var i = 0; i < current_project.skills.length; i++)
        skills_element.append(this.getSkillChipHtml(current_project.skills[i]));
    this.element.find("#project-deadline").text(current_project.deadline);
    
    this.element.find("#project-employer-photo").attr("src", current_project_employer.photo);
    this.element.find("#project-employer-name").text(current_project_employer.name);
    this.element.find("#project-employer-email").html(this.getEmployerEmailHtml(current_project_employer.email));
    this.element.find("#project-employer-phone").html(this.getEmployerPhoneHtml(current_project_employer.phone));
    var project = this;
    this.element.find("#project-see-employer-profile").on("click", function() {
        project.app.openUserProfile(current_project_employer.email);
    });

    this.element.find("#project-budget").text(current_project.budget + "$");
    this.element.find("#project-num-bids").text(current_project.bids.length);
    this.element.find("#project-avg-bid").text(this.getAvgBid(current_project.bids));
    this.element.find("#project-min-bid").text(this.getMinBid(current_project.bids));

    this.element.find("#bid-slider-input").attr("min", Math.floor(current_project.budget / 10));
    this.element.find("#bid-slider-input").attr("max", Math.floor(current_project.budget));
    this.element.find("#bid-slider-input").val(Math.floor(current_project.budget));
    this.slider = document.getElementById("bid-slider");
    noUiSlider.create(this.slider, {
        start: [current_project.budget],
        step: 1,
        orientation: 'horizontal',
        connect: [true, false],
        range: {
            'min': Math.floor(current_project.budget / 10),
            'max': current_project.budget
        }
    });
    // Keep slider and range input in sync
    this.slider.noUiSlider.on('update', function(values, handle) {
        project.element.find("#bid-slider-input").val(values[0]);
    });
    this.element.find("#bid-slider-input").on("change", function() {
        project.slider.noUiSlider.set($(this).val());
    });

    // Add the current bids
    var cur_bids_element = this.element.find("#current-bids-list");
    for (var i = 0; i < current_project.bids.length; i++) {
        var bid = current_project.bids[i];
        cur_bids_element.append(this.getBidHtml(bid));
    }
}

Project.prototype.placeBid = function() {
    if (!g_logged_in_user) {
        Materialize.toast("You have to be logged in to place a bid", 4000);
        return;
    }
    if (g_logged_in_user == current_project_employer.email) {
        Materialize.toast("You can't bid on your own project", 4000);
        return;
    }
    var bid = {
        user_email: g_logged_in_user,
        value: Math.floor(this.element.find("#bid-slider-input").val()),
        comment: this.element.find("#bid-comment").val()
    };
    var project = this;
    $.ajax({
        type: "POST",
        url: backend_server + "/bids",
        dataType: "json",
        data: {
            project_id: current_project._id.$oid,
            bid: JSON.stringify(bid)
        }
    }).done(function(result) {
        if (result.status != "success")
            throw "Error adding bid";
        Materialize.toast("Bid added successfuly", 4000);
        current_project.bids.push(bid);
        // Reload page
        project.app.setPage("project");
    });
}

Project.prototype.getAvgBid = function(bids) {
    if (bids.length == 0)
        return "No bids yet";
    var avg_bid = 0;
    for (var i = 0; i < bids.length; i++)
        avg_bid += bids[i].value;
    avg_bid /= bids.length;
    return avg_bid.toFixed(2) + "$";
}

Project.prototype.getMinBid = function(bids) {
    if (bids.length == 0)
        return "No bids yet";
    var min_bid = bids[0].value;
    for (var i = 1; i < bids.length; i++)
        min_bid = Math.min(min_bid, bids[i].value);
    return min_bid + "$";
}

Project.prototype.getSkillChipHtml = function(skill) {
    return `<div class="chip"> ` + skill + ` </div>`;
}

Project.prototype.getEmployerEmailHtml = function(email) {
    return `<i class="fa fa-envelope"></i> &nbsp; ` + email;
}

Project.prototype.getEmployerPhoneHtml = function(phone) {
    return `<i class="fa fa-lg fa-mobile"></i> &nbsp;` + phone;
}

Project.prototype.getBidHtml = function(bid) {
    var user = users[bid.user_email];

    return `
    <li class="collection-item avatar">
        <img src="` + user.photo + `" class="profile-picture circle">
        <a href="#" class="waves-effect" onclick="app.openUserProfile('` + user.email + `')">
          <span class="title font-size-18 strong">` + user.name + `</span>
        </a>
        <p class="strong">` + bid.value + `$</p>
        <p>
          ` + bid.comment + `
        </p>
    </li>
    `;
}