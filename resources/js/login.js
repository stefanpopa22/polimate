'use strict';

function UsersManager() {
    this.element = undefined;
    this.init();
}

UsersManager.prototype.init = function() {
    // Update last_active field of logged in user every minute.
    setInterval(this.updateLastActive(), 60 * 1000);
}

UsersManager.prototype.updateLastActive = function() {
    if (g_logged_in_user) {
        var new_last_active = new Date().toString();
        $.ajax({
            type: "POST",
            url: backend_server + "/users/" + g_logged_in_user,
            dataType: "json",
            data: {
                update: "True",
                last_active: new_last_active
            }
        });
        users[g_logged_in_user].last_active = new_last_active;
    }
}

UsersManager.prototype.setElement = function(element) {
    this.element = element;
}

UsersManager.prototype.createUser = function() {
    var name_element = $(this.element).find("#register_name");
    var password_element = $(this.element).find("#register_password");
    var email_element = $(this.element).find("#register_email");

    if (name_element.is(':invalid') || password_element.is(':invalid') || email_element.is(':invalid'))
        return;

    var password_hash = sha256(password_element.val());

    $.ajax({
        type: "POST",
        url: backend_server + "/users",
        dataType: "json",
        data: {
            name: name_element.val(),
            password_hash: password_hash,
            email: email_element.val(),
            last_active: new Date().toString()
        }
    }).done(function(result) {
        if (result.status == "error") {
            Materialize.toast(result.message, 4000);
            email_element.removeClass("valid");
            email_element.addClass("invalid");
        }
        else if (result.status == "success") {
            Materialize.toast("You registered successfully.", 4000);
            setTimeout(function() {app.setPage('login');}, 500);
        }
    });
}

UsersManager.prototype.login = function() {
    var email_element = $(this.element).find("#login_email");
    var password_element = $(this.element).find("#login_password");

    if (email_element.is(':invalid') || password_element.is(':invalid'))
        return;

    var password_hash = sha256(password_element.val());
    var users_manager = this;

    $.ajax({
        type: "POST",
        url: backend_server + "/login",
        dataType: "json",
        data: {
            email: email_element.val(),
            password_hash: password_hash
        }
    }).done(function(result) {
        if (result.status == "error") {
            Materialize.toast(result.message, 4000);
        }
        else if (result.status == "success") {
            users_manager._loggedInCb(email_element.val(), result.token);
        }
    });
}

UsersManager.prototype.tryLogin = function() {
    var user = Cookies.get('polimate_user');
    var token = Cookies.get('polimate_token');

    if (user && token)
        this._loggedInCb(user, token);
    else
        this._updateLoginView();
}

UsersManager.prototype.logout = function() {
    Cookies.remove('polimate_user');
    Cookies.remove('polimate_token');

    g_logged_in_user = "";
    g_token = undefined;

    this._updateLoginView();
    Materialize.toast("You logged out successfully!", 4000);
}

UsersManager.prototype._loggedInCb = function(user, token) {
    Cookies.set('polimate_user', user);
    Cookies.set('polimate_token', token);

    g_logged_in_user = user;
    g_token = token;

    Materialize.toast("Welcome, " + users[g_logged_in_user].name + "!", 4000);
    setTimeout(function() {app.setPage(defaultPage);}, 500);
    this.updateLastActive();
    this._updateLoginView();
}

UsersManager.prototype._updateLoginView = function() {
    var login_btn = $("#login_btn");
    var logout_btn = $("#logout_btn");
    var navbar_user_logo = $("#navbar_user_logo");

    if (g_logged_in_user) {
        login_btn.hide();
        logout_btn.show();

        var user_logo = users[g_logged_in_user]["photo"];
        if (!user_logo)
            user_logo = 'resources/img/default_profile_picture.png';
        navbar_user_logo.find("img").attr("src", user_logo);
        navbar_user_logo.show();
    }
    else {
        login_btn.show();
        logout_btn.hide();
        navbar_user_logo.hide();
    }
}