'use strict';

function PostProjectForm(element) {
    this.element = element;
    this.init();
}

PostProjectForm.prototype.init = function() {
    $(this.element).find('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        format: 'ddd, dd mmm yyyy',
        closeOnSelect: false // Close upon selecting a date,
    });
};

PostProjectForm.prototype.createProject = function() {
    var title = $(this.element).find("#project-title");
    var description = $(this.element).find("#post-project-description");
    var deadline = $(this.element).find("#deadline");
    var budget = $(this.element).find("#project-budget");
    
    var skills = [];
    $(this.element)
        .find(".skills-input-chips > .chip")
        .each(function(i, el) {
            skills.push($(el).contents().get(0).nodeValue);
        });

    if (title.is(':invalid') || description.is(':invalid') || deadline.is(':invalid') || budget.is(":invalid")
        || budget.val() <= 0)
        return;

    var project_data = {
        title: title.val(),
        details: description.val(),
        deadline: deadline.val(),
        skills: JSON.stringify(skills),
        budget: parseInt(budget.val())
    };

    // Add the project to the database
    $.ajax({
        method: 'POST',
        url: backend_server + "/projects",
        dataType: "json",
        data: project_data,
        xhrFields: {
            withCredentials: true,
        }
    }).done(function(result) {
        if (result.status == "error") {
            Materialize.toast(result.message, 4000);
        }
        else if (result.status == "success") {
            app.post_project_form._postedProjectCb(title.val());
        }
    });
};

PostProjectForm.prototype._postedProjectCb = function(title) {
    Materialize.toast("Project " + title + " posted!", 4000);
    setTimeout(function() {app.setPage("projectsList");}, 500);
};