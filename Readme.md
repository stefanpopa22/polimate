## Installation and running
#### Running the backend
1. Install pip and virtualenv.

        $ sudo apt install python-pip python-dev build-essential
        $ sudo apt install virtualenv
        $ sudo pip install --upgrade pip

2. Enter the virtual environment

        $ cd polimate_backend
        $ source ./start.sh

3. Run with ```python server.py```.
#### Running the frontend
1. Install nodejs and npm.
    
        $ sudo apt-get update
        $ sudo apt install nodejs
        $ sudo apt install nodejs-legacy
        $ sudo apt install npm
    
2. Run ```npm start```
3. You will find the page at ```http://localhost:8000```.

### Design doc + TODO list
https://docs.google.com/document/d/1_5FkiyFUTljVPhvZ4YtDg10nsFjr1TwUf1oIC-de1oY/edit#
